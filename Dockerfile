FROM openjdk:11
VOLUME [ "/pixel/log", "/pixel/images" ]
COPY ./target/pixel-app-0.0.1-SNAPSHOT.jar /app/app.jar
ENTRYPOINT ["java","-jar","/app/app.jar"]
EXPOSE 8080
