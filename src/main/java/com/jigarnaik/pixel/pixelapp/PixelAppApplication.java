package com.jigarnaik.pixel.pixelapp;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
@Log4j2
public class PixelAppApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(PixelAppApplication.class, args);
    }

}
