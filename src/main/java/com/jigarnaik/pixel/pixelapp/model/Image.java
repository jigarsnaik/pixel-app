package com.jigarnaik.pixel.pixelapp.model;

import com.jigarnaik.pixel.pixelapp.exception.NotSupportedFormatException;
import com.jigarnaik.pixel.pixelapp.util.ImageUtil;
import lombok.extern.log4j.Log4j2;

import java.io.InputStream;

@Log4j2
public class Image {

    private InputStream inputStream;
    private String fileName;

    public Image(InputStream inputStream, String fileName) {
        this.inputStream = inputStream;
        this.fileName = fileName;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileExtension() {
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    public void resize(int width, int height, String destinationPath) throws NotSupportedFormatException {
        log.info("Resizing image {} ", fileName);
        ImageUtil.resize(this, width, height, destinationPath);
        log.info("Image {} resized successfully", fileName);

    }
}
