package com.jigarnaik.pixel.pixelapp.controller;

import com.jigarnaik.pixel.pixelapp.exception.NotSupportedFormatException;
import com.jigarnaik.pixel.pixelapp.model.Image;
import com.jigarnaik.pixel.pixelapp.service.ImageResizeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/image")
@CrossOrigin
@Log4j2
public class ImageController {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
    private final ImageResizeService imageResizeService;

    public ImageController(ImageResizeService imageResizeService) {
        this.imageResizeService = imageResizeService;
    }

    @GetMapping("/health")
    public String health() {
        log.info("Health looks fine.");
        return "Service is running.";
    }

    @PostMapping("/resize")
    public ResponseEntity<byte[]> resize(@RequestParam("files") List<MultipartFile> files) throws IOException {
        var requestId = FORMATTER.format(LocalDateTime.now());
        log.info("Request received to resize {} images, request Id {}", files.size(), requestId);
        List<Image> images = new ArrayList<>();
        for (MultipartFile e : files) {
                images.add(new Image(e.getInputStream(), e.getOriginalFilename()));

        }
        log.info("All images uploaded successfully.");
        File file = imageResizeService.resize(images, requestId);
        byte[] data = Files.readAllBytes(file.getAbsoluteFile().toPath());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentLength(data.length);
        headers.setContentType(new MediaType("application", "zip"));
        headers.setCacheControl("no-cache");
        headers.set(HttpHeaders.CONTENT_DISPOSITION, String.format("inline; filename=\"" + file.getName() + "\""));
        log.info("Success : Sending Response : File Name : {} , length : {}", file.getName(), data.length);
        return new ResponseEntity<>(data, headers, HttpStatus.OK);
    }

}
