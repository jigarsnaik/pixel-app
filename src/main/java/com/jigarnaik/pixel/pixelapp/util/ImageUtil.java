package com.jigarnaik.pixel.pixelapp.util;

import com.jigarnaik.pixel.pixelapp.exception.NotSupportedFormatException;
import com.jigarnaik.pixel.pixelapp.model.Image;
import lombok.extern.log4j.Log4j2;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Log4j2
public class ImageUtil {

    private ImageUtil() {
    }

    /**
     * Method will resize image and save it to the destination directory provided.
     *
     * @param image               to be resized
     * @param width               pixel
     * @param height              pixel
     * @param destinationFilePath after resize image will be saved to this directory
     * @throws IOException
     */
    public static void resize(Image image, int width, int height, String destinationFilePath) throws NotSupportedFormatException {
        try {
            BufferedImage bi = ImageIO.read(image.getInputStream());
            BufferedImage outputImage = new BufferedImage(width, height, bi.getType());
            Graphics2D g2d = outputImage.createGraphics();
            g2d.drawImage(bi, 0, 0, width, height, null);
            g2d.dispose();
            Files.createDirectories(Path.of(destinationFilePath));
            ImageIO.write(outputImage, image.getFileExtension(), new File(destinationFilePath + File.separator + image.getFileName()));
        } catch (Exception e) {
            throw new NotSupportedFormatException("Exception while resizing image file " + image.getFileName());
        }
    }
}
