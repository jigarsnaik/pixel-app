package com.jigarnaik.pixel.pixelapp.exception;

public class NotSupportedFormatException extends Exception {

    public NotSupportedFormatException(String message) {
        super(message);
    }
}
