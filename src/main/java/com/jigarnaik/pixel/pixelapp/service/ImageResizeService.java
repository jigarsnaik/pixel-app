package com.jigarnaik.pixel.pixelapp.service;

import com.jigarnaik.pixel.pixelapp.exception.NotSupportedFormatException;
import com.jigarnaik.pixel.pixelapp.model.Image;
import com.jigarnaik.pixel.pixelapp.util.ZipUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Service
@Log4j2
public class ImageResizeService {

    private static final int WIDTH = 1000;
    private static final int HEIGHT = 1000;

    @Value("${app.output-file-path}")
    private String outputFilePath;


    public File resize(List<Image> images, String requestId) {
        var destinationPath = outputFilePath + File.separator + requestId;
        long start = System.currentTimeMillis();
        images.parallelStream().forEach(image -> {
            try {
                image.resize(WIDTH, HEIGHT, destinationPath);
            } catch (NotSupportedFormatException e) {
                log.error("Format not supported, other image resize will continue. Image Name : " + image.getFileName());
            }
        });
        long end = System.currentTimeMillis();
        log.info("Total time taken to resize {} images is {} seconds", images.size(), (end - start) / 1000.000);
        ZipUtil.zipDirectory(destinationPath);
        return new File(destinationPath + File.separator + requestId + ".zip");
    }

}
