# pixel-app

Pixel server component for image resizing

## Getting started
##### Framework used
- Spring Boot
##### SSH into AWS instance
`ssh -i <private key with permission 0400> ec2-user@<ip-address>`

#####Service hosted at http://54.255.128.196/

## Docker commands

#### How the service is deployed?
- Service is deployed on AWS EC2 Micro instance using docker. 
- Custome AMI with Docker CE created. 
- Using custom created AMI to create new EC2 Micro Instance. 
- Docker image has been pushed to docker hub repository
- The image from Docker hub repository has been pulled to EC2 instance. 
- To start the application docker continaer has been started in detached mode on AWS EC2 instance. 
##### To tail container logs 
`docker logs --follow <Container ID>`

Build 
`docker build -t jigarsnaik/pixel-web . `

Push to dockerhub
`docker push jigarsnaik/pixel-web`

Run locally
`docker run -p 80:8080 jigarsnaik/pixel-web:latest`