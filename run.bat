#build project and run tests
mvn clean package

#build image
docker build -t pixel-app .

#delete container if already present 
docker container stop pixel-app

#delete container if already present 
docker container rm pixel-app

#create container from image
docker run -p 8080:8080 -t -it --name pixel-app pixel-app

#delete container
docker container rm pixel-app

#delete image
docker image rmi pixel-app

#tag image
docker tag pixel-app:latest jigarsnaik/pixel-app:1.0

#push image to github repository
docker push jigarsnaik/pixel-app

docker run -p 8080:8080 -d --name pixel-app jigarsnaik/pixel-app